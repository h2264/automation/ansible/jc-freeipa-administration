# jc-freeipa-administration

Ansible playbook for FreeIPA administrative tasks

## Assumptions
- [ ] FreeIPA server has been configured
- [ ] Target VM is reachable by SSH

## System Prep
```bash
#!/bin/bash
# Clone repository recursively to include the make submodule
git clone --recursive git@gitlab.com:h2264/automation/ansible/jc-pve-templates.git

# Ensure that ssh access is preconfigured to proxmox host
vi ~/.ssh/config

# Initialise FreeIPA server details
cat << 'EOF' >> .env
_TARGET_IPV4= # TARGET HOST IPv4
_ANSIBLE_EXEC_INVENTORY=$(_TARGET_IPV4),

# Set ansible vars
_IPA_USR= # FreeIPA user
_IPA_PSW= # FreeIPA password
_TARGET_HOSTNAME= # Target host hostname
_TARGET_DOMAIN= # Target host domain name

_ANSIBLE_EXEC_ENVVARS=-e ipa_usr=$(_IPA_USR) -e ipa_psw='$(_IPA_PSW)' -e target_hostname='$(_VM_HOSTNAME)' -e target_domain='$(_VM_DOMAIN)' -e target_ip=$(_TARGET_IPV4)
EOF
```

## Execution
```bash
#!/bin/bash

# Register host to FreeIPA server
make override_registration
```
This executes the ansible playbook `./src/ipaclient-registration.yaml`. Logical flow are as follows.
- Update package manager cache and install dependencies.
- Configure ipa client and register host to freeipa server
- If `app_cert=true`, install host application cert.

```bash
#!/bin/bash

# Resign host from FreeIPA server
make override_resignation
```
This executes the ansible playbook `./src/ipaclient-resignation.yaml`. Logical flow are as follows.
- Acquire kerberos ticket
- Remove host entry from FreeIPA server
- Remove DNS record from FreeIPA server
- Unset FreeIPA client configuration from host
- Delete kerberos ticket

## Variables
### Shell
Override through `make $VARIABLENAME=$VALUE $MAKETARGET`. E.g. `make _ANSIBLE_SRC=./src ansible-lint`
| Name | Affected make targets | Comment |
| --- | --- | --- |
| _ANSIBLE_VENV | all | Virtualenv directory. Path where ansible is stored and referenced. |
| _ANSIBLE_SRC | all | Ansible source code root directory |
| _ANSIBLE_REQUIREMENTS | `ansible-do-systemprep` | Pip requirements file. |
| _ANSIBLE_LINT_YAML_RULES | `ansible-do-lint` | Pyyaml rules. Used for linting yaml files. |
| _ANSIBLE_EXEC_PLAYBOOK | `ansible-do-exec` | Relative path of the playbook to be executed. |
| _ANSIBLE_EXEC_INVENTORY | `ansible-do-exec` | Relative path of the playbook to be executed. Can be also set as `$IPV4,` |
| _ANSIBLE_EXEC_ENVVARS | `ansible-do-exec` | Placeholder for ansible env vars. |
| _ANSIBLE_EXEC_TAGS | `ansible-do-exec` | Set ansible tags. Only tasks or playbooks with these tags are executed. |
|||

### Ansible env vars
| Name | Comment |
| --- | --- |
| `app_cert` | Boolean. Determines if Host application certificates are to be created |
| `ipa_certpath` | Defaults to `/etc/pki`. Location where the application certs reside. |
| `ipa_domain` | Default FreeIPA domain. |
| `ipa_usr` | FreeIPA service account. Must have access to register hosts, administer DNS records and certificates. |
| `ipa_psw` | FreeIPA service account password |
| `ipa_server` | FQDN of FreeIPA server. |
| `ipa_realm` | Kerberos Realm |
| `ntp_server` | Defaults to `pool.ntp.org`. NTP remote server. |
| `nameserver_ipv4` | Homelab nameserver. |
| `target_hostname` | Hostname of target Host. |
| `target_domain` | Domain name of target Host. |
| `target_ip` | IPv4 of target Host.|
|||

## CICD integration
### System prep
### Repository Layout
### Payloads
### Usage
