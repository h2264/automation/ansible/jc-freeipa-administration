_ANSIBLE_EXEC_PLAYBOOK=ipaclient-resignation.yaml
_ANSIBLE_EXEC_INVENTORY=
_ANSIBLE_EXEC_ENVVARS=-e ipa_usr='' -e ipa_psw='' -e target_hostname='' -e target_domain='' -e target_ip=''
_ANSIBLE_LINT_YAML_RULES=-d "{extends: relaxed, rules: {line-length: {max: 400}}}"

# DATA PAYLOAD
-include .env

override_registration:
	$(MAKE) _ANSIBLE_EXEC_PLAYBOOK=ipaclient-registration.yaml  ansible-exec

override_resignation:
	$(MAKE) _ANSIBLE_EXEC_PLAYBOOK=ipaclient-resignation.yaml  ansible-exec