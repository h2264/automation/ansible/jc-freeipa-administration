# include base makefile
include .make/base.mk

# include makefiles to be inherited
include .make/ansible.mk

# include override residing in current repository
-include Override.mk

